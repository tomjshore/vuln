package com.tomjshore.vuln.cvss;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CvssCalculatorTest {

    private CvssCalculator calculator;

    @BeforeEach
    void setUp(){
        calculator = new CvssCalculator();
    }

    @ParameterizedTest
    @CsvSource({
            "N, H, L, R, U, L, L, L, 4.6",
            "N, H, L, R, U, H, H, H, 7.1",
            "N, H, N, R, U, L, N, L, 4.2",
            "L, L, L, N, C, H, N, L, 7.3"
    })
    void testCalculateReturnsCorrectWhenGivenBaseScoreVector(String av, String ac, String pr, String ui, String s, String c,
                                                             String i, String a, float score){

        //given
        AttackVector attackVector = AttackVector.get(av);
        AttackComplexity attackComplexity = AttackComplexity.get(ac);
        PrivilegesRequired privilegesRequired = PrivilegesRequired.get(pr);
        UserInteractions userInteractions = UserInteractions.get(ui);
        Scope scope = Scope.get(s);
        Confidentiality confidentiality = Confidentiality.get(c);
        Integrity integrity = Integrity.get(i);
        Availability availability = Availability.get(a);


        //when
        float result = calculator.calculate(attackVector, attackComplexity, privilegesRequired, userInteractions, scope, confidentiality, integrity, availability);

        //then
        assertEquals(score, result, 0.1);
    }
}
