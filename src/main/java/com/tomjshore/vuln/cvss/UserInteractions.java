package com.tomjshore.vuln.cvss;

import java.util.Arrays;

public enum UserInteractions {
    REQUIRED("R"),
    NONE("N");

    private final String value;

    UserInteractions(String value){
        this.value = value;
    }

    @Override
    public String toString(){
        return this.value;
    }

    public static UserInteractions get(String value){
        return Arrays.stream(UserInteractions.values())
                .filter(userInteractions -> userInteractions.toString().equals(value))
                .findFirst().orElseThrow();
    }
}
