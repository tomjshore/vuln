package com.tomjshore.vuln.cvss;

import java.util.Arrays;

public enum Confidentiality {
    HIGH("H"),
    LOW("L"),
    NONE("N");

    private final String value;

    Confidentiality(String value){
        this.value = value;
    }

    @Override
    public String toString(){
        return this.value;
    }

    public static Confidentiality get(String value){
        return Arrays.stream(Confidentiality.values())
                .filter(confidentiality -> confidentiality.toString().equals(value))
                .findFirst().orElseThrow();
    }
}
