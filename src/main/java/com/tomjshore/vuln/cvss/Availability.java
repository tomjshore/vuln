package com.tomjshore.vuln.cvss;

import java.util.Arrays;

public enum Availability {
    HIGH("H"),
    LOW("L"),
    NONE("N");

    private final String value;

    Availability(String value){
        this.value = value;
    }

    @Override
    public String toString(){
        return this.value;
    }

    public static Availability get(String value){
        return Arrays.stream(Availability.values())
                .filter(availability -> availability.toString().equals(value))
                .findFirst().orElseThrow();
    }
}
