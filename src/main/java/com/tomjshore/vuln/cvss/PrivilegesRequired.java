package com.tomjshore.vuln.cvss;

import java.util.Arrays;

public enum PrivilegesRequired {
    HIGH("H"),
    LOW("L"),
    NONE("N");

    private final String value;

    PrivilegesRequired(String value){
        this.value = value;
    }

    @Override
    public String toString(){
        return this.value;
    }

    public static PrivilegesRequired get(String value){
        return Arrays.stream(PrivilegesRequired.values())
                .filter(privilegesRequired -> privilegesRequired.toString().equals(value))
                .findFirst().orElseThrow();
    }
}
