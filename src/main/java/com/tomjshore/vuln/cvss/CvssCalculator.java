package com.tomjshore.vuln.cvss;

import java.math.RoundingMode;
import java.util.Map;
import org.apache.commons.math3.util.Precision;

public class CvssCalculator {

    private final Map<AttackVector, Float> attackVectorMap = Map.of(AttackVector.NETWORK, 0.85F, AttackVector.ADJACENT, 0.62F, AttackVector.LOCAL, 0.55F, AttackVector.PHYSICAL, 0.2F);
    private final Map<AttackComplexity, Float> attackComplexityMap = Map.of(AttackComplexity.LOW, 0.77F, AttackComplexity.HIGH, 0.44F);
    private final Map<UserInteractions, Float> userInteractionsMap = Map.of(UserInteractions.REQUIRED, 0.62F, UserInteractions.NONE, 0.85F);
    private final Map<Confidentiality, Float> confidentialityMap = Map.of(Confidentiality.HIGH, 0.56F, Confidentiality.LOW, 0.22F, Confidentiality.NONE, 0F);
    private final Map<Integrity, Float> integrityMap = Map.of(Integrity.HIGH, 0.56F, Integrity.LOW, 0.22F, Integrity.NONE, 0F);
    private final Map<Availability, Float> availabilityMap = Map.of(Availability.HIGH, 0.56F, Availability.LOW, 0.22F, Availability.NONE, 0F);


    public float calculate(AttackVector attackVector, AttackComplexity attackComplexity, PrivilegesRequired privilegesRequired, UserInteractions userInteractions, Scope scope, Confidentiality confidentiality, Integrity integrity, Availability availability) {

        float impactSubScore = impactSubScore(confidentiality, integrity, availability);
        float impact = impact(scope, impactSubScore);

        if(impact <= 0F){
            return 0F;
        }

        float exploitability = exploitability(attackVector, attackComplexity, privilegesRequired, userInteractions, scope);

        return baseScore(scope, exploitability, impact);
    }


    /**
     * 1 - ( (1 - Confidentiality) × (1 - Integrity) × (1 - Availability) )
     * @param confidentiality
     * @param integrity
     * @param availability
     * @return the impact sub score or iss
     */
    private float impactSubScore(Confidentiality confidentiality, Integrity integrity, Availability availability){
        float confidentialityScore = confidentialityMap.get(confidentiality);
        float integrityScore = integrityMap.get(integrity);
        float availabilityScore = availabilityMap.get(availability);

        return 1 - ((1 - confidentialityScore) * (1 - integrityScore) * (1 - availabilityScore));
    }

    /**
     * if scope is unchanged 6.42 × ISS
     * if scope is changed 7.52 × (ISS - 0.029) - 3.25 × (ISS - 0.02)^15
     * @param scope
     * @param impactSubScore
     * @return the impact
     */
    private float impact(Scope scope, float impactSubScore){
        if(Scope.UNCHANGED.equals(scope)){
            return 6.42F * impactSubScore;
        }
        return 7.52F * (impactSubScore - 0.029F) - 3.25F * Double.valueOf(Math.pow((impactSubScore - 0.02F),15D)).floatValue();
    }

    private float privilegesRequired(PrivilegesRequired privilegesRequired, Scope scope){
        switch (privilegesRequired){
            case LOW:
                if(scope.equals(Scope.UNCHANGED)){
                    return 0.62F;
                }
                return 0.68F;
            case HIGH:
                if(scope.equals(Scope.UNCHANGED)){
                    return 0.27F;
                }
                return 0.5F;
            default:
                return 0.85F;
        }
    }

    /**
     * 	8.22 × AttackVector × AttackComplexity × PrivilegesRequired × UserInteraction
     * @param attackVector
     * @param attackComplexity
     * @param privilegesRequired
     * @param userInteractions
     * @param scope
     * @return the exploitability
     */
    private float exploitability(AttackVector attackVector, AttackComplexity attackComplexity, PrivilegesRequired privilegesRequired, UserInteractions userInteractions, Scope scope){
        float attackVectorScore = attackVectorMap.get(attackVector);
        float attackComplexityScore = attackComplexityMap.get(attackComplexity);
        float privilegesRequiredScore = privilegesRequired(privilegesRequired, scope);
        float userInteractionsScore = userInteractionsMap.get(userInteractions);

        return 8.22F * attackVectorScore * attackComplexityScore * privilegesRequiredScore * userInteractionsScore;
    }

    /**
     * if scope is unchanged Roundup (Minimum ((Impact + Exploitability), 10))
     * if scope is changed Roundup (Minimum (1.08 × (Impact + Exploitability), 10)
     * @param scope
     * @param exploitability
     * @param impact
     * @return the baseScore
     */
    private float baseScore(Scope scope, float exploitability, float impact){
        if(Scope.UNCHANGED.equals(scope)){
            return roundUp(Math.min((impact + exploitability), 10F));
        }

        return roundUp(Math.min((1.08F * (impact + exploitability)), 10F));
    }

    /**
     * Rounds by ceiling to one decimal place
     * For example, roundup (4.02) returns 4.1;
     * and roundup (4.00) returns 4.0.
     * To ensure consistent results across programming languages and hardware,
     * .
     * @param number to round up
     * @return rounded number
     */
    private float roundUp(float number){
        return Precision.round(number,1, RoundingMode.CEILING.ordinal());
    }


}
