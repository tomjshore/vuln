package com.tomjshore.vuln.cvss;

import java.util.Arrays;

public enum AttackVector {
    NETWORK("N"),
    ADJACENT("A"),
    LOCAL("L"),
    PHYSICAL("P");

    private final String value;

    AttackVector(String value){
        this.value = value;
    }

    @Override
    public String toString(){
        return this.value;
    }

    public static AttackVector get(String value){
        return Arrays.stream(AttackVector.values())
                .filter(attackVector -> attackVector.toString().equals(value))
                .findFirst().orElseThrow();
    }
}
