package com.tomjshore.vuln.cvss;

import java.util.Arrays;

public enum AttackComplexity {
    HIGH("H"),
    LOW("L");

    private final String value;

    AttackComplexity(String value){
        this.value = value;
    }

    @Override
    public String toString(){
        return this.value;
    }

    public static AttackComplexity get(String value){
        return Arrays.stream(AttackComplexity.values())
                .filter(attackComplexity -> attackComplexity.toString().equals(value))
                .findFirst().orElseThrow();
    }
}
