package com.tomjshore.vuln.cvss;

import java.util.Arrays;

public enum Integrity {
    HIGH("H"),
    LOW("L"),
    NONE("N");

    private final String value;

    Integrity(String value){
        this.value = value;
    }

    @Override
    public String toString(){
        return this.value;
    }

    public static Integrity get(String value){
        return Arrays.stream(Integrity.values())
                .filter(integrity -> integrity.toString().equals(value))
                .findFirst().orElseThrow();
    }
}
