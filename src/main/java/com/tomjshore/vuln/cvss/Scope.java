package com.tomjshore.vuln.cvss;

import java.util.Arrays;

public enum Scope {
    UNCHANGED("U"),
    CHANGED("C");

    private final String value;

    Scope(String value){
        this.value = value;
    }

    @Override
    public String toString(){
        return this.value;
    }

    public static Scope get(String value){
        return Arrays.stream(Scope.values())
                .filter(scope -> scope.toString().equals(value))
                .findFirst().orElseThrow();
    }
}
